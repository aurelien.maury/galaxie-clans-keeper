import unittest
import GLXClansKeeper


class TestGLXClansKeeper(unittest.TestCase):
    def setUp(self) -> None:
        self.clans_keeper = GLXClansKeeper.Keeper()


    def test_lang(self):
        self.assertEqual('en', self.clans_keeper.lang)
        self.clans_keeper.lang = None
        self.assertEqual('en', self.clans_keeper.lang)

        self.assertRaises(TypeError, setattr, self.clans_keeper, 'lang', 42)

    def test_charset(self):
        self.assertEqual('utf-8', self.clans_keeper.charset)
        self.clans_keeper.charset = None
        self.assertEqual('utf-8', self.clans_keeper.charset)

        self.assertRaises(TypeError, setattr, self.clans_keeper, 'charset', 42)

    # def test_run(self):
    #     self.clans_keeper.run()


if __name__ == '__main__':
    unittest.main()
